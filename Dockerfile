FROM ruby:2.6.0-slim-stretch

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        build-essential \
        default-libmysqlclient-dev \
        nodejs && \
    rm -rf /var/lib/apt/lists/*


WORKDIR /opt/bite

COPY Gemfile Gemfile.lock /opt/bite/
RUN bundle install

COPY . .

EXPOSE 3000

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
